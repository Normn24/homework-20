const slides = document.querySelectorAll('.image-to-show');
const stopButton = document.getElementById('stopButton');
const resumeButton = document.getElementById('resumeButton');
const timer = document.getElementById('countdown');

let current = 0;
let stopSlide = false;
let slideTime;

resumeButton.style.display = "none";

function showSlide(index) {
  slides.forEach((slide, i) => {
    slide.style.display = i === index ? 'block' : 'none';
  });
}

function startSlideshow() {
  slideTime = setInterval(() => {
    showSlide(current);
    current = (current + 1) % slides.length;
    startTimer(3);
  }, 3000);
}

function startTimer(seconds) {
  let second = seconds;
  timer.textContent = second;
  const intervalTimer = setInterval(() => {
    second -= 1;
    timer.textContent = second;
    if (second <= 1) {
      clearInterval(intervalTimer);
    }
  }, 1000);
}

function stopSlideshow() {
  clearInterval(slideTime);
  stopSlide = false;
  resumeButton.style.display = "block";
}

function resumeSlideshow() {
  if (!stopSlide) {
    startSlideshow();
    stopSlide = true;
  }
}


startSlideshow();
stopButton.addEventListener('click', stopSlideshow);
resumeButton.addEventListener('click', resumeSlideshow);

